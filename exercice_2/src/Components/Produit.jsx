import React from "react";
import CardContent from "material-ui/Card/CardContent";
import Typography from "material-ui/Typography/Typography";

const style = {
	typography: {
		display: "inline-block"
	}
};

const Produit = ({ nom, prix, localisation, caracteristiques, loisirs }) => (
	<div>
		<CardContent>
			<div>
				<Typography type="title" style={style.typography}>
					Nom : &nbsp;
				</Typography>
				{nom}
			</div>
			<div>
				<Typography type="title" style={style.typography}>
					Prix : &nbsp;
				</Typography>
				{prix}
			</div>
			<div>
				<Typography type="title" style={style.typography}>
					Localisation : &nbsp;
				</Typography>
				{localisation}
			</div>
			<div>
				<Typography type="title" style={style.typography}>
					Caractéristiques : &nbsp;
				</Typography>
				{caracteristiques}
			</div>
			<div>
				<Typography type="title" style={style.typography}>
					loisirs : &nbsp;
				</Typography>
				{loisirs}
			</div>
		</CardContent>
	</div>
);

export default Produit;
