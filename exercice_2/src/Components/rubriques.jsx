import React from "react";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography/Typography";
import Rubrique from "./rubrique";
import Divider from "material-ui/Divider/Divider";
import Grid from "material-ui/Grid/Grid";
import grey from "material-ui/colors/grey";

const style = {
	paper: {
		backgroundColor: grey["200"],
		margin: 5
	}
};

const Rubriques = ({ products, titre }) => (
	<Paper style={style.paper}>
		<Grid container justify="center">
			<Grid item>
				<Typography type="display2">{titre}</Typography>
			</Grid>
		</Grid>
		<Divider />
		<Grid container justify="center">
			{products.map(product => (
				<Grid item>
					<Rubrique Product={product} />
				</Grid>
			))}
		</Grid>
	</Paper>
);

export default Rubriques;
