import React from "react";
import Card from "material-ui/Card";
import Produit from "./Produit";
import CardMedia from "material-ui/Card/CardMedia";
import Dialog from "material-ui/Dialog/Dialog";
import DialogTitle from "material-ui/Dialog/DialogTitle";
import DialogContent from "material-ui/Dialog/DialogContent";
import { info } from "../utils/data";
import Typography from "material-ui/Typography/Typography";

const style = {
	card: {
		maxWidth: 380,
		width: 480,
		margin: 5
	},
	media: {
		height: 200
	},
	rubrique: {
		display: "inline-block",
		cursor: "pointer"
	}
};

class Rubrique extends React.Component {
	constructor(props) {
		super(props);
		this.state = { open: false };
		this.onClick = this.onClick.bind(this);
	}
	onClick() {
		this.setState({ open: !this.state.open });
	}
	render() {
		const { Product } = this.props;
		return (
			<div onClick={this.onClick} style={style.rubrique}>
				<Dialog open={this.state.open}>
					<DialogTitle>Informations</DialogTitle>
					<DialogContent>
						<Typography paragraph>{info}</Typography>
					</DialogContent>
				</Dialog>
				<Card style={style.card}>
					<CardMedia image={Product.image} style={style.media} />
					<Produit key={Product.name} {...Product} />
				</Card>
			</div>
		);
	}
}

export default Rubrique;
