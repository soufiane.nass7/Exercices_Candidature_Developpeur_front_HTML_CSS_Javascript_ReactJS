export const appartements = [
	{
		nom: "appar1",
		prix: 2000000,
		localisation: "rabat",
		caracteristiques: "grand",
		loisirs: "jardin",
		image: "/static/images/app1.jpg"
	},
	{
		nom: "appar2",
		prix: 4000000,
		localisation: "salé",
		caracteristiques: "2 chambre , salon, cuisine",
		loisirs: "jardin",
		image: "/static/images/app2.jpg"
	},
	{
		nom: "appar3",
		prix: 5000000,
		localisation: "casa",
		caracteristiques: "3 chambre , salon, cuisine",
		loisirs: "jardin",
		image: "/static/images/app3.jpg"
	}
];

export const magasins = [
	{
		nom: "magasin1",
		prix: 200000,
		localisation: "rabat",
		caracteristiques: "Surface superieure à 500 m",
		loisirs: "*",
		image: "/static/images/mag1.jpg"
	},
	{
		nom: "magasin2",
		prix: 4000000,
		localisation: "salé",
		caracteristiques: "Surface superieure à 900 m",
		loisirs: "*",
		image: "/static/images/mag2.jpeg"
	},
	{
		nom: "magasin3",
		prix: 70000,
		localisation: "casa",
		caracteristiques: "Surface superieure à 200 m",
		loisirs: "*",
		image: "/static/images/mag3.jpg"
	}
];

export const villas = [
	{
		nom: "villa1",
		prix: 200000000,
		localisation: "rabat",
		caracteristiques: "Surface superieure à 19.500 m",
		loisirs: "jardin",
		image: "/static/images/villa1.jpg"
	},
	{
		nom: "villa2",
		prix: 400000000,
		localisation: "salé",
		caracteristiques: "Surface superieure à 12.500 m",
		loisirs: "picine",
		image: "/static/images/villa2.jpg"
	},
	{
		nom: "villa3",
		prix: 700000000,
		localisation: "casa",
		caracteristiques: "Surface superieure à 9.500 m",
		loisirs: "jardin",
		image: "/static/images/villa3.jpg"
	}
];

export const info =
	"Quand j’étais petit je rêvais d’un grand appartement dans un superbe immeuble.\
Doté d'une terrasse vue mer avec une petite j’ardin qui contiens plusieurs types des fleurs, J’imaginais aussi une grande salle à manger ouverte sur la cuisine pour régaler et recevoir plein de monde.\
La cuisine est l’une des pièces les plus importantes de la maison, elle est également le refuge qui accueille tous nos petits moments intimes et inoubliables en famille. J’imaginais une cuisine charmante avec un design américain avec des couleurs les plus tendances de la saison « Jaune citron ou bleu pastel... ».\
	Un grand salon clair confortable et chaleureux bien coloré, qui symbolise une bonne entente d’amicale.\
J’imaginais aussi suffisamment de chambres pour accueillir famille et amis.\
";
