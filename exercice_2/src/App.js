import React, { Component } from "react";
import Contents from "./Containers/Contents";

class App extends Component {
	render() {
		return <Contents />;
	}
}

export default App;
