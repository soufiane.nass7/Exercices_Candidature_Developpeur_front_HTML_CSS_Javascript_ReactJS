import React from "react";
import { appartements, magasins, villas } from "../utils/data";
import Rubriques from "../Components/rubriques";

class Contents extends React.Component {
	render() {
		return (
			<div>
				<Rubriques products={appartements} titre="Appartements" />
				<Rubriques products={villas} titre="Villas" />
				<Rubriques products={magasins} titre="Magasins" />
			</div>
		);
	}
}

export default Contents;
